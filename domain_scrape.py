#!/usr/bin/python3
import sys
import html2text
from googlesearch import search
import nltk
from nltk.tokenize import word_tokenize
import requests
import urllib

from nameparser.parser import HumanName

def get_human_names(text):
    tokens = word_tokenize(text)
    pos = nltk.pos_tag(tokens)
    sentt = nltk.ne_chunk(pos, binary = False)
    person_list = []
    person = []
    name = ""
    for subtree in sentt.subtrees(filter=lambda t: t.label() == 'PERSON'):
        for leaf in subtree.leaves():
            person.append(leaf[0])
        if len(person) > 1: #avoid grabbing lone surnames                                
            for part in person:
                name += part + ' '
            if name[:-1] not in person_list:
                person_list.append(name[:-1])
            name = ''
        person = []
    return (person_list)

def get_article(url):
    names_list = []
    f = requests.get(url)
    txt = html2text.html2text(f.text)
    names = get_human_names(txt)
    for name in names:
        first_last = HumanName(name).first + ' ' + HumanName(name).last
        print(first_last)
        
if __name__ == '__main__':
    if len(sys.argv) >= 2:
        url = ' '.join(sys.argv[1:])
        url_bytes = str.encode(url)
        get_article(url_bytes)
    else:
        print("python domain_scrape.py <keyword>")
