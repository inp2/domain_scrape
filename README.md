# domain_scrape

A data scraping tool used for extracting names and important entities from a given website.
The tool is initially provided an entities and fetches the website. Once fetched, the web
page is extracted, parseed, reformatted, and relies on basic natural language processing
to determine important entities. This information is return as a list of words.

## Prerequisites

`sys, html2text, googlesearch, nltk, nltk.tokenize, requests, urllib, nameparser.parser`

## Example

`python3 domain_scrape.py <website>`

`python3 domain_scrape.py https://www.donaldjtrump.com`
